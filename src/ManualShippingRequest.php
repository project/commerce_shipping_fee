<?php

namespace Drupal\commerce_shipping_fee;

/**
 *
 * @package Drupal\commerce_shipping_fee
 */
abstract class ManualShippingRequest implements ManualShippingRequestInterface {
  /**
   * The configuration array from a CommerceShippingMethod.
   *
   * @var array
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  public function setConfig(array $configuration) {
    $this->configuration = $configuration;
  }
}
