<?php

namespace Drupal\commerce_shipping_fee;

/**
 *
 * @package Drupal\commerce_shipping_fee
 */
interface ManualShippingRequestInterface {

  /**
   * Set the request configuration.
   *
   * @param array $configuration
   *   A configuration array from a CommerceShippingMethod.
   */
  public function setConfig(array $configuration);

}
