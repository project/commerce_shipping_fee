<?php

namespace Drupal\commerce_shipping_fee;

/**
 * @package Drupal\commerce_shipping_fee
 */
interface ManualShippingRateRequestInterface extends ManualShippingRequestInterface {

  /**
   * The name of the logger channel to use throughout this module.
   */
  const LOGGER_CHANNEL = 'commerce_logistics_service';

}
