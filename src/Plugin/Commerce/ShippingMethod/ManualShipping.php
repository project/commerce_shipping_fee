<?php

namespace Drupal\commerce_shipping_fee\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_currency_resolver\CurrentCurrencyInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the manual shipping method.
 *
 * @CommerceShippingMethod(
 *  id = "commerce_shipping_fee",
 *  label = @Translation("Commerce manual shipping"),
 * )
 */
class ManualShipping extends ShippingMethodBase {

  /**
   * @var \Drupal\commerce_currency_resolver\CurrentCurrencyInterface
   */
  protected $currentCurrency;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_currency_resolver\CurrentCurrencyInterface $current_currency
   *   The currency manager.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The rounder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, CurrentCurrencyInterface $current_currency, RounderInterface $rounder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->currentCurrency = $current_currency;
    $this->rounder = $rounder;
    $company_name = $this->configuration['company_name']??"";
    $this->services['default'] = new ShippingService('default', $company_name);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_currency_resolver.current_currency'),
      $container->get('commerce_price.rounder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $currency_code = \Drupal::service('commerce_currency_resolver.current_currency')
      ->getCurrency();
    return [
        'freight_options' => [
          'freight_base_amount' => [
            'number' => 0,
            'currency_code' => $currency_code,
          ],
          'freight_additional_amount' => [
            'number' => 0,
            'currency_code' => $currency_code,
          ],
          'freight_base_weight' => 0,
        ],
        'options' => [
          'tracking_url' => '',
          'round' => PHP_ROUND_HALF_UP,
        ],
        'services' => ['default'],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['company_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shipping company name'),
      '#default_value' => $this->configuration['company_name'],
      '#required' => TRUE,
    ];
    $form['is_free'] = [
      '#type' => 'checkbox',
      '#title' => t('Free Shipping'),
      '#default_value' => $this->configuration['is_free'],
    ];
    $form['freight_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Freight Options'),
      '#states' => [
        'visible' => [
          ':input[name="plugin[0][target_plugin_configuration][commerce_shipping_fee][is_free]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['freight_options']['freight_base_amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Freight Base Amount'),
      '#description' => $this->t('Fixed price, or can be used for the first weight fixed price.'),
      '#default_value' => $this->configuration['freight_options']['freight_base_amount'],
    ];
    $form['freight_options']['freight_base_weight'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.1,
      '#title' => $this->t('Freight Base Weight'),
      '#description' => $this->t('Fixed weight, or can be used for the first weight.'),
      '#default_value' => $this->configuration['freight_options']['freight_base_weight'] ?? 0,
    ];
    $form['freight_options']['freight_additional_amount'] = [
      '#type' => 'commerce_price',
      '#description' => $this->t('Additional freight beyond the base weight (per weight unit).'),
      '#title' => $this->t('Additional price(per weight unit).'),
      '#default_value' => $this->configuration['freight_options']['freight_additional_amount'],
    ];
    $form['freight_options']['round'] = [
      '#type' => 'select',
      '#title' => $this->t('Freight Round'),
      '#description' => $this->t('It is used to round the shipping costs.'),
      '#options' => [
        PHP_ROUND_HALF_UP => 'ROUND HALF UP',
        PHP_ROUND_HALF_DOWN => 'ROUND HALF DOWN',
        PHP_ROUND_HALF_EVEN => 'ROUND HALF EVEN',
        PHP_ROUND_HALF_ODD => 'ROUND HALF ODD',
      ],
      '#default_value' => $this->configuration['options']['round'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['company_name'] = $values['company_name'];
      $this->configuration['is_free'] = $values['is_free'];
      $this->configuration['freight_options']['freight_base_amount'] = $values['freight_options']['freight_base_amount'];
      $this->configuration['freight_options']['freight_base_weight'] = $values['freight_options']['freight_base_weight'];
      $this->configuration['freight_options']['freight_additional_amount'] = $values['freight_options']['freight_additional_amount'];
      $this->configuration['freight_options']['round'] = $values['freight_options']['round'];
      $form_state->setValue($form['#parents'], $values);
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  public function calculateRates(ShipmentInterface $shipment) {
    $freight = [];
    $currency_code = $this->currentCurrency->getCurrency();
    $price = new Price(0, $currency_code);
    if (empty($this->configuration['is_free'])) {
      $amount = $this->configuration['freight_options']['freight_additional_amount'];
      $basic_amount = $this->configuration['freight_options']['freight_base_amount'];
      $basic_weight = $this->configuration['freight_options']['freight_base_weight'];
      if (!empty($shipment->getWeight())) {
        $weight = $shipment->getWeight()->convert('kg')->getNumber();
        $additional_weight = $weight - $basic_weight;
        $total_amount = [
          'number' => $amount['number'] * $additional_weight + $basic_amount['number'],
          'currency_code' => $amount['currency_code'],
        ];
        $amount_price = Price::fromArray($total_amount);
        $round = !empty($this->configuration['freight_options']['round'])
          ? $this->configuration['freight_options']['round']
          : PHP_ROUND_HALF_UP;
        $price = $this->rounder->round($amount_price, $round);
      }
      $freight[] = new ShippingRate([
        'shipping_method_id' => $this->parentEntity->id(),
        'service' => $this->services['default'],
        'amount' => $price,
      ]);
    }
    return $freight;
  }

}
