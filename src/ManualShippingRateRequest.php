<?php

namespace Drupal\commerce_shipping_fee;

/**
 * Represents the manual shipping rate request.
 */
class ManualShippingRateRequest extends ManualShippingRequest implements ManualShippingRequestInterface {

  public function setConfig(array $configuration) {
  }

}
