COMMERCE SHIPPING FEE
=====================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration

INTRODUCTION
------------

Provides shipping fee auxiliary calculation functionality for Drupal Commerce.

为Drupal Commerce提供运费辅助计算功能。

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

按照您通常安装已贡献的Drupal模块的方式进行安装。
请参阅：https://www.drupal.org/documentation/install/modules-themes/modules-8

CONFIGURATION
-------------

* Configure shipping methods
  Configure existing logistics types or new logistics types in "/admin/commerce/shipping-methods".
  Select "Commerce manual shipping" in "Plugin".
* Freight calculation:
  1. Set fixed shipping costs and weights.
  2. For those exceeding the fixed shipping weight,
   enter the weight of the portion exceeding the fixed shipping weight and the shipping cost per unit weight for the excess.
   Based on the weight exceeding the shipping weight, the shipping cost is calculated as:
   additional shipping cost per kilogram x weight exceeding the fixed shipping weight + fixed shipping cost (fixed shipping cost) = total shipping cost.
* 配置运输方式
  在“/admin/commerce/shipping methods”中配置现有物流类型或新物流类型。
  在“Plugin”中选择“Commerce manual shipping”。
* 运费计算：
  1.设置固定运费、运重。
  2.对于超出固定运重的，输入超出固定运重部分的重量，以及对于超出部分的单位重量的运费。
    基于超出运重的重量部分，按：每千克额外运费 x 超出固定运重的重量 + 固定运费(固定运重的运费) = 总运费，得出运费。
